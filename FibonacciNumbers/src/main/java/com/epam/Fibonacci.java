package com.epam;

import java.io.*;
import java.util.*;


public class Fibonacci {

  static boolean isFibonacci(int arrayMember) {

    int p = 5 * arrayMember * arrayMember + 4; // 5n*n + 4
    int sqrtOfP = (int) Math.sqrt(p);
    int r = 5 * arrayMember * arrayMember - 4; // 5n*n - 4
    int sqrtOfR = (int) Math.sqrt(r);

    return ((p == sqrtOfP * sqrtOfP) || (r == sqrtOfR * sqrtOfR));
  }


  public static void main(String[] args) {

    Scanner scan = new Scanner(System.in);

    System.out.println("Введіть межі інтервалу: ");
    System.out.print("x1 = ");
    int start = scan.nextInt();
    System.out.print("x2 = ");
    int end = scan.nextInt();
    int length = end - start + 1;

    int[] Array = new int[length];
    Array[0] = start;

    int count = start + 1;
    for (int j = 1; j < length; j++) {

      Array[j] = count;
      count = count + 1;

    }

    for (int j = 0; j < length; j++) {
      System.out.print(Array[j] + " ");
    }

    int oddTotal = 0;
    int evenTotal = 0;
    System.out.println();
    System.out.println("Непарні числа від х1 до х2: ");
    for (int j = 0; j < length; j++) {
      if (Array[j] % 2 != 0) {
        System.out.print(Array[j] + " ");
        oddTotal += Array[j];
      }

    }
    System.out.println();
    System.out.println("Парні числа від х2 до х1: ");
    for (int j = length - 1; j >= 0; j--) {
      if (Array[j] % 2 == 0) {
        System.out.print(Array[j] + " ");
        evenTotal += Array[j];
      }

    }
    System.out.println();
    System.out.println("Сума парних чисел у проміжку: " + evenTotal);
    System.out.println("Сума непарних чисел у проміжку: " + oddTotal);

    // fibonacci sequence
    // int[] fArray = new int [length];
    // fArray[0] = 1;
    // fArray[1] = 1;

    // for(int i=2; i < end; i++){
    //    fArray[i] = fArray[i-1] + fArray[i-2];
    // }
    System.out.println("Fibonacci числа: ");

    int numFromFile = 0;
    int chosen;
    System.out.println("Кількість: ");
    System.out.print("Зчитати з клавіатури (1) чи з файлу(2) ? - ");
    int countFibonacci = 0;

    boolean wasCorrectEntered = false;

    do {
      try {
        chosen = scan.nextInt();
        if (chosen == 1) {
          System.out.print("Введіть кількість чисел Фібоначчі:");
          countFibonacci = scan.nextInt();
          wasCorrectEntered = true;
        }
        if (chosen == 2) {
          try (Scanner reader = new Scanner(new File("D:\\text.txt"))) {

            numFromFile = reader.nextInt();
          } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.out.println("Файл не знайдено!");
          }
          countFibonacci = numFromFile;
          wasCorrectEntered = true;
        }

        int[] fibonacciArray = new int[countFibonacci];
        int iterator = 0;

        int N = 0;
        while (iterator != countFibonacci) {
          if (isFibonacci(N) == true) {
            fibonacciArray[iterator] = N;
            iterator++;
          }
          N++;
        }

        if (fibonacciArray[countFibonacci - 1] % 2 == 0) {
          System.out.println(fibonacciArray[countFibonacci - 1] + "  - найбільше парне число");
          System.out.println(fibonacciArray[countFibonacci - 2] + "  - найбільше непарне число");

        } else {
          System.out.println(fibonacciArray[countFibonacci - 1] + "  - найбільше непарне число");
          System.out.println(fibonacciArray[countFibonacci - 2] + "  - найбільше парне число");
        }

        for (int j = 0; j < countFibonacci; j++) {
          System.out.print(fibonacciArray[j] + " ");
        }

        int countEvenInFibonacci = 0;
        int countOddInFibonacci = 0;

        for (int j = 0; j < countFibonacci; j++) {
          if (fibonacciArray[j] % 2 == 0) {
            countEvenInFibonacci++;
          } else {
            countOddInFibonacci++;
          }
        }

        System.out.println();
        System.out
            .println("Парні числа становлять: " + (double)countEvenInFibonacci/countFibonacci * 100 +
                "% в усьому проміжку чисел Фібоначчі");

        System.out.println("Непарні числа становлять: " + (double)countOddInFibonacci/countFibonacci * 100 +
                "% в усьому проміжку чисел Фібоначчі");


      } catch (Exception e) {
        // e.printStackTrace();
        System.out.println("Або 1 або 2!!!!!!!");
      }
    } while (!wasCorrectEntered);

    System.out.println("........................");
    System.out.println("Try with resources usage");
    System.out.println("........................");

    try (TestResources object = new TestResources()) {
      object.testMethod();
    } catch (Exception e) {
      System.out.println("Опрацювання catch");

    }


  }


}





